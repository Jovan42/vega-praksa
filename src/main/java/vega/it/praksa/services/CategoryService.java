package vega.it.praksa.services;

import vega.it.praksa.model.dtos.CategoryDto;
import vega.it.praksa.model.dtos.CategoryListDto;

public interface CategoryService extends GenericService<CategoryDto, CategoryListDto, Long> {
}
